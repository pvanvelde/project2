import numpy as np
import random


def calc_pol(n_beads, n, n_pol, alfa_low, alfa_up, T_dim, PERM, random_walk):
    """Calculates configuration of n_pol polymers up to a length of n_beads

    Parameters:
    ----------
    n_beads: integer > 2
        Number of maximum beats in a polymer
    n: integer > 0
        Number of possible angles
    n_pol: integer > 1
        Amount of polymers
    alfa_low: float >= 0
        Number to compute lower limit of PERM algorithm
    alfa_up: float > alfa_low
        Number to compute upper limit of PERM algorithm
    T_dim: float > 0
        Non-dimensional temperature

    Results:
    --------
    end_end_array: array of size (n_beads, n_poll + amount of enriching - 0.5 times amount of pruning)
        Distance of bead to center bead, for every bead of every simulated polymer
    polweight_array: (n_beads, n_poll + amount of enriching - 0.5 times amount of pruning)
        Individual weights of every cell in end_end_array
    """

    # Allocate necessary arrays
    pos_stack, pos_array, polweight_stack, polweight_array, end_end_stack, \
        end_end_array = allocate(n_beads, n_pol)

    # Initialise position of first two beads and their weighting factor
    pos_stack, polweight_stack = init(n_pol, pos_stack, polweight_stack)

    # Initialise some variables
    n_enrich = 0  # amount of times enrichment was done
    n_prune = 0  # amount of times pruning was done
    flag1 = 1   # flag used to escape while loop
    j = 2   # index to loop over n_beads
    m = 0   # index to count the number of delete columns by pruning

    # Calculate total polymers and their weights
    while flag1 == 1:
        k = 0
        flag2 = 1

        # Calculate next bead of polymer
        while flag2 == 1:

            # Calculate next bead of every polymer, and apply PERM
            pos_stack, pos_array, polweight_stack, polweight_array, \
            end_end_stack, end_end_array, j, k, m, n_prune, n_enrich = \
                perm(pos_stack, pos_array, polweight_stack, polweight_array,
                     end_end_stack, end_end_array, j, k, m, n, n_prune, n_enrich,
                     T_dim, alfa_up, alfa_low, PERM, random_walk)

            # Loop over all polymers, and abort while loop if all polymers have a new bead
            k = k + 1
            if k >= int(pos_stack.shape[1] / 2):
                flag2 = 0

        # Loop over all n_beads, and abort while loop if all polymers have their final length
        if pos_stack.size == 0 or j == n_beads - 1:
            flag1 = 0
            print('stack equal to zero at number of beads = ', j + 1)
        if PERM == True:
            print('number of polymers = ', polweight_stack.shape[1])
        print('polymers have lenght: ', j+1, 'beads')
        j = j + 1

    # Append the two end2end matrices, the position matrices, and their weights, an
    end_end_array = np.c_[end_end_array, end_end_stack]
    polweight_array = np.c_[polweight_array, polweight_stack]
    pos_array = np.c_[pos_array, pos_stack]

    print('n_enrich = ', n_enrich)
    print('n_prune = ', n_prune)

    return end_end_array, polweight_array, pos_array


def allocate(n_beads, n_pol):
    """Allocates necessary arrays to store positions and weights of beads of polymers

    Parameters:
    ----------
    n_beads: integer > 2
        Number of maximum beats in a polymer
    n_pol: integer > 1
        Amount of polymers

    Results:
    --------
    pos_stack: array of size (n_beads, n_pol*2)
        Zero array, to store position of beads in
    pos_array: array of size (n_beads, 2)
        Zero array, to store position of beads in
    polweight_stack: array of size (n_beads, n_pol)
        Zero array, to store weights in
    polweight_array: array of size (n_beads)
        Zero array, to store weights in
    end_end_stack: array of size (n_beads, n_pol)
        Zero array, to store end-to-end distance in
    end_end_array: array of size (n_beads, 1)
        Zero array, to store end-to-end distance in
    """

    pos_stack = np.zeros([n_beads, n_pol * 2])
    pos_array = np.zeros([n_beads, 2])

    polweight_stack = np.zeros([n_beads, n_pol])
    polweight_array = np.zeros([n_beads])

    end_end_stack = np.zeros([n_beads, n_pol])
    end_end_array = np.zeros([n_beads, 1])
    return pos_stack, pos_array, polweight_stack, polweight_array, end_end_stack, end_end_array


def init(n_pol, pos_stack, polweight_stack):
    """Initialise the first positions and their weight of n_pol polymers

    Parameters:
    ----------
    n_pol: integer > 1
        Amount of polymers
    pos_stack: array of size (n_beads, n_pol*2)
        Zero array, to store positions of the beads of n_pol polymers
    polweight_stack: array of size (n_beads, n_pol)
        Zero array, to store weights of beads of n_pol polymers

    Results:
    --------
    pos_stack: array of size (n_beads, n_pol*2)
        Array with positions of the beads of n_pol polymers
    polweight_stack: array of size (n_beads, n_pol)
        Array with the weights of beads of n_pol polymers
    """
    m = np.arange(0, 2 * n_pol, 2)

    # Give the second beads of all polymers position [1, 0]
    pos_stack[1, m] = 1
    polweight_stack[0:2, :] = 1
    return pos_stack, polweight_stack


def perm(pos_stack, pos_array, polweight_stack, polweight_array, end_end_stack, end_end_array,
         j, k, m, n, n_prune, n_enrich, T_dim, alfa_up, alfa_low, PERM, random_walk):
    """Preforms PERM algorithm to calculate next bead (k) of polymer (j)

    Parameters:
    ----------
    pos_stack: array of size (n_beads, (n_pol - m + n_enrich)*2 )
        Array with positions of the beads of polymers which are still growing
    pos_array: array of size (n_beads, m+1)
        Array with positions of the beads of polymers which stopped growing
    polweight_stack: array of size (n_beads, n_pol - m + n_enrich)
        Array with the weights of beads of polymers which are still growing
    polweight_array: array of size (n_beads, m+1)
        Array with the weights of beads of n_pol polymers which stopped growing
    end_end_stack: array of size (n_beads, n_pol - m + n_enrich)
        Array with end-to-end distances of polymers which are still growing
    end_end_array: array of size (n_beads, m+1)
        Array with end-to-end distances of polymers which stopped growing
    j: integer > 2
        Number to indicate which bead grows onto polymer
    k: integer >= 0
        Number to indicate at which polymer bead number j grows
    m: integer >=0
        Number to indicate amount of pruning (times 0.5)
    n: integer > 0
        Number of possible angles
    n_prune: integer >=0
        Amount of times pruning has been preformed
    n_enrich: integer >=0
        Amount of times enriching has been preformed
    T_dim: float > 0
        Non-dimensional temperature
    alfa_up: float >= 0
        Number to compute lower limit of PERM algorithm
    alfa_low: float > alfa_low
        Number to compute upper limit of PERM algorithm

    Results:
    --------
    pos_stack: array of size (n_beads, (n_pol - m + n_enrich)*2 )
        Array with positions of the beads of polymers which are still growing
    pos_array: array of size (n_beads, (m+1)*2)
        Array with positions of the beads of polymers which stopped growing
    polweight_stack: array of size (n_beads, n_pol - m + n_enrich)
        Array with the weights of beads of polymers which are still growing
    polweight_array: array of size (n_beads, m+1)
        Array with the weights of beads of n_pol polymers which stopped growing
    end_end_stack: array of size (n_beads, n_pol - m + n_enrich)
        Array with end-to-end distances of polymers which are still growing
    end_end_array: array of size (n_beads, m+1)
        Array with end-to-end distances of polymers which stopped growing
    j: integer > 2
        Number to indicate which bead grows onto polymer
    k: integer >= 0
        Number to indicate at which polymer bead number j grows
    m: integer >=0
        Number to indicate amount of pruning (times 0.5)
    n: integer > 0
        Number of possible angles
    n_prune: integer >=0
        Amount of times pruning has been preformed
    n_enrich: integer >=0
        Amount of times enriching has been preformed
    """
    # Avoid PERM and random_walk are true together
    if random_walk == True and PERM == True:
        print('Error: random walk and PERM cannot be true both, change this is simulation parameters.')
        exit()

    # Add next bead to polymer
    pos_stack[:, 2 * k:2 * k + 2], polweight_stack[j, k], end_end_stack[j, k] \
        = add_bead(pos_stack[:, 2 * k:2 * k + 2], n, T_dim, polweight_stack[j - 1, k], j, random_walk)

    # Calculate upper and lower limits for PERM algorithm
    if PERM == True:

        uplim, lowlim = limits(polweight_stack, alfa_up, k, alfa_low, j)

        # Conditions for enriching
        if polweight_stack[j, k] > uplim:
            n_enrich = n_enrich + 1
            polweight_stack, pos_stack, end_end_stack, k \
                = enrich(polweight_stack, pos_stack, end_end_stack, k)

        # Conditions for pruning
        if polweight_stack[j, k] < lowlim:
            n_prune = n_prune + 1
            pos_stack, polweight_stack, pos_array, polweight_array, end_end_stack, end_end_array, k, m \
                = prune(polweight_stack, pos_stack, pos_array, polweight_array,
                        end_end_stack, end_end_array, k, m)

    return pos_stack, pos_array, polweight_stack, polweight_array, \
        end_end_stack, end_end_array, j, k, m, n_prune, n_enrich


def add_bead(pos, n, T_dim, polweight, j, random_walk):
    """Adds one bead to polymer (j), according to the Rosenblaum method

    Parameters:
    ----------
    pos: array of size (n_beads, 2)
        Array with positions of the beads of polymer (j)
    n: integer > 0
        Number of possible angles
    T_dim: float > 0
        Non-dimensional temperature
    polweight: array of size (n_beads, 1)
        Array with the weights of the beads of polymer (j)
    j: integer > 2
        Number to indicate which bead grows onto polymer
    Results:
    --------
    pos: array of size (n_beads, 2)
        Array with positions of the beads of polymer (j)
    polweight: array of size (n_beads, 1)
        Array with the weights of the beads of polymer (j)
    end_end: float > 0
        Value which gives the distance from first bead to newly added bead
    """

    # Calculate (random) possible angles of next bead
    phi_0 = np.linspace(0, 2 * np.pi - (2 / n) * np.pi, n)
    phi = phi_0 + (1 - 2 * np.random.rand(n)) * np.pi / n

    # Calculate new positions
    pos_new = np.zeros([n, 2])
    pos_new[:, 0] = pos[j - 1, 0] + np.cos(phi)
    pos_new[:, 1] = pos[j - 1, 1] + np.sin(phi)

    # Calculate distance between new bead and beads
    rx = np.zeros([n, j])
    ry = np.zeros([n, j])
    for i in range(n):
        rx[i, :] = pos_new[i, 0] - pos[0:j, 0]
        ry[i, :] = pos_new[i, 1] - pos[0:j, 1]
    r = np.sqrt(rx ** 2 + ry ** 2)
    r[r == 0] = np.nan
    E = np.sum(4 * 0.25 * ((0.8 / r) ** 12 - (0.8 / r) ** 6), axis=1)

    # Calculate small weights for random walk
    if random_walk == True:
        w_jl = np.ones(n)  # small weights (per angle)
    else:
        w_jl = np.exp(-E / T_dim)  # small weights (per angle)
    w_l = np.sum(w_jl)  # big weight per added bead (sum of angle weights)

    # Adjust zero weights to use them for probability in random choice angle
    flag = 0
    if w_l == 0:
        w_jl = np.ones(w_jl.shape) * 10 ** (-50)
        w_l = np.sum(w_jl)
        flag = 1

    # Calculate polweight, and adjust for the previous step
    cst_dep = 1 / (0.75 * n)  # multiplication constant for L dependence perm
    polweight = polweight * w_l * cst_dep  # weight per polymer off by our algorithm
    if flag == 1:
        polweight = 0

    # Calculate new position of next bead
    P = w_jl / w_l
    final_phi = np.random.choice(phi, p=P)
    pos[j] = np.array([[pos[j - 1, 0] + np.cos(final_phi), pos[j - 1, 1] + np.sin(final_phi)]])
    end_end = np.sqrt(pos[j, 0] ** 2 + pos[j, 1] ** 2)
    return pos, polweight, end_end


def enrich(polweight_stack, pos_stack, end_end_stack, k):
    """Performs the enriching part of the PERM algorithm:
    Adds copy of current polymer to whole stack, and
    gives both polymer half of their current weight.

    Parameters:
    ----------
    polweigh_stack: array of size (n_beads, changing number X)
        Array with the weights of the beads of X polymers
    pos_stack: array of size (n_beads, changing number X times 2)
        Array with positions of the beads of X polymers
    end_end_stack: array of size (n_beads, changing number X)
        Array with the end-to-end distance of the beads of X polymers
    k: integer >= 0
        Number to indicate which polymer is enriched

    Results:
    polweigh_stack: array of size (n_beads, changing number X)
        Array with the weights of the beads of X polymers
    pos_stack: array of size (n_beads, changing number X times 2)
        Array with positions of the beads of X polymers
    end_end_stack: array of size (n_beads, changing number X)
        Array with the end-to-end distance of the beads of X polymers
    --------
    """
    # Half the weights of current polymer to keep total weight constant
    polweight_stack[:, k] = 0.5 * polweight_stack[:, k]

    # Duplicate current polymer (weights, position and end2end)
    polweight_stack = np.insert(polweight_stack, k, polweight_stack[:, k], axis=1)
    pos_stack = np.insert(pos_stack, 2 * k, pos_stack[:, 2 * k + 1], axis=1)
    pos_stack = np.insert(pos_stack, 2 * k, pos_stack[:, 2 * k + 1], axis=1)
    end_end_stack = np.insert(end_end_stack, k, end_end_stack[:, k], axis=1)
    k = k + 1

    return polweight_stack, pos_stack, end_end_stack, k


def limits(polweight_stack, alfa_up, k, alfa_low, j):
    """Calculates upper and lower limit which decide when to prune or enrich in PERM algorithm

    Parameters:
    ----------
    polweigh_stack: array of size (n_beads, changing number X)
        Array with the weights of the beads of X polymers
    alfa_low: float >= 0
        Number to compute lower limit of PERM algorithm
    k: integer >= 0
        Number to indicate at which polymer next bead grows
    alfa_up: float > alfa_low
        Number to compute upper limit of PERM algorithm
    j: integer > 2
        Number to indicate which bead grows next onto polymer

    Results:
    --------
    upper: float > 0
        Number to indicate when to enrich
    lower: 0 < float < upper
        Number to indicate when to prune
    """

    # Find all nonzero elements of stack & pw_matrix, of bead j, sum and average:
    avg_weight = np.average(polweight_stack[j, :k+1])
    weight3 = polweight_stack[2, k]     # Average weights of first 3 beads

    # Limit computation
    upper = (alfa_up * avg_weight) / weight3
    lower = (alfa_low * avg_weight) / weight3

    return upper, lower


def prune(polweight_stack, pos_stack, pos_array, polweight_array, end_end_stack, end_end_array, k, m):
    """Performs the pruning part of the PERM algorithm:
    Half of the times it doubles the weight of the current polymer, and
    half of the times it moves the current polymer from stack to final array.

    Parameters:
    polweigh_stack: array of size (n_beads, changing number X)
        Array with the weights of the beads of X polymers
    pos_stack: array of size (n_beads, changing number X times 2)
        Array with positions of the beads of X polymers
    pos_array: array of size (n_beads, (m+1)*2
        Array with positions of the beads of polymers which stopped growing
    polweight_array: array of size (n_beads, m+1)
        Array with the weights of beads of n_pol polymers which stopped growing
    end_end_stack: array of size (n_beads, changing number X)
        Array with the end-to-end distance of the beads of X polymers
    end_end_array: array of size (n_beads, m+1)
        Array with end-to-end distances of polymers which stopped growing
    k: integer >= 0
        Number to indicate which polymer is pruned
    m: integer >=0
        Number to indicate amount of pruning (times 0.5)
    ----------

    Results:
    --------
    pos_stack: array of size (n_beads, changing number X times 2)
        Array with positions of the beads of X polymers
    polweigh_stack: array of size (n_beads, changing number X)
        Array with the weights of the beads of X polymers
    pos_array: array of size (n_beads, (m+1)*2
        Array with positions of the beads of polymers which stopped growing
    polweight_array: array of size (n_beads, m+1)
        Array with the weights of beads of n_pol polymers which stopped growing
    end_end_stack: array of size (n_beads, changing number X)
        Array with the end-to-end distance of the beads of X polymers
    end_end_array: array of size (n_beads, m+1)
        Array with end-to-end distances of polymers which stopped growing
    k: integer >= 0
        Number to indicate which polymer is pruned
    m: integer >=0
        Number to indicate amount of pruning (times 0.5)
    """

    # Calculate random number between 0 and 1
    b = random.random()
    if b < 0.5:
        # Multiply weights of current polymer by 2 to keep total weight the same
        polweight_stack[:, k] = polweight_stack[:, k] * 2
    else:
        # Move pruned polymers from stacks into arrays (for weights, positions and end2end)
        polweight_array = np.c_[polweight_array, polweight_stack[:, k]]
        polweight_stack = np.delete(polweight_stack, k, axis=1)
        pos_array = np.c_[pos_array, pos_stack[:, 2 * k]]
        pos_array = np.c_[pos_array, pos_stack[:, 2 * k + 1]]
        pos_stack = np.delete(pos_stack, [2 * k, 2 * k + 1], axis=1)
        end_end_array = np.c_[end_end_array, end_end_stack[:, k]]
        end_end_stack = np.delete(end_end_stack, k, axis=1)
        k = k - 1
        m = m + 1

    return pos_stack, polweight_stack, pos_array, polweight_array, end_end_stack, end_end_array, k, m
