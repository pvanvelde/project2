# Project2

Computational physics project 2: Monte Carlo simulations of polymers interacting according the Lennard-Jones potential.

Main.py : main script to run the simulation.

simfunctions.py : functions used to simulate configuration of polymers.

processfunctions.py : Process configuration of polymers and their weights into figures and perform bootstrap to calculate errors

By:

Luuk Balkenende & Pieter van Velde



Last modified: 15-5-2019