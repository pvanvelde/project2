# Import functions
from simfunctions import calc_pol
from processfunctions import exp_calc

# Simulation parameters
n_beads = 250  # number of beads
n = 6  # number of angles
n_pol = 200  # number of polymers
alfa_up = 2  # alfa to compute upper limit for PERM
alfa_low = 1.2  # alfa to compute lower limit for PERM

# Physical parameters
T_dim = 10 # T * (Kb / eps)  # Non-dimensional temperature

# Choose algorithm
PERM = False    # Put on True if PERM algorithm is desired
random_walk = False     # Put on True if random walk is desired


# Simulate configuration of polymers and their weights
end_end_array, polweight_array, pos_array = calc_pol(n_beads, n, n_pol, alfa_low, alfa_up, T_dim, PERM, random_walk)

# Process configuration of polymers and their weights into figures
# and perform bootstrap to calculate errors.
exp_calc(polweight_array, end_end_array, n_beads, pos_array)
