import numpy as np
import matplotlib
from matplotlib import pyplot as plt


def exp_calc(polweight_array, end_end_array, n_beads, pos_array):
    """Calculates the end to end distances with taking into account their weights, and
    makes a figure of the mean values of those distances against number of beads.

    Parameters:
    ----------
    polweight_array: array of size (n_beads, n_poll + amount of enriching - 0.5 times amount of pruning)
        Array with all weights of all beads of all polymers
    end_end_array: array of size (n_beads, n_poll + amount of enriching - 0.5 times amount of pruning)
        Distance of bead to center bead, for every bead of every simulated polymer
    n_beads: integer > 2
        Number of maximum beats in a polymer

    Results:
    --------
    Figure: end to end distance against number of beads
    """
    # Compute mean values and standard deviation using bootstrap.
    exp_value_mean, exp_value_std = bootstrap(polweight_array, end_end_array, n_beads)
    rad_gyr = radiusgyration(pos_array)
    rad_gyr_mean, rad_gyr_std = bootstrap(polweight_array, rad_gyr, n_beads)
    exp_value_mean[0:2] = [0, 1]
    rad_gyr_mean[0] = 0

    # Compute population size
    popsize = polsize(end_end_array)
    

    # Plot data
    font = {'family': 'DejaVu Sans',
            'weight': 'normal',
            'size': 17}
    matplotlib.rc('font', **font)

    # Create fit for end to end distance
    a = np.arange(0, n_beads - 1, 1)
    q = np.polyfit(np.log(a[1::]), np.log(exp_value_mean[1::] ** 2), 1)
    print('Fitting parameters for end2end distance -> ', q)
    xx = np.linspace(0, 250, 1000)
    yy = np.exp(q[1]) * xx ** q[0]

    # Plot end to end distance
    plt.figure(1)
    plt.xscale("log")
    plt.yscale("log")
    plt.plot(xx, yy, 'r--', label="Fit", linewidth=3)
    plt.xlabel(r'$N_{beads}$')
    plt.ylabel(r'$R^2$ $[\sigma^2]$')

    plt.xscale("log")
    plt.yscale("log")
    plt.errorbar(a, exp_value_mean[0:n_beads - 1] ** 2, fmt='x', xerr=None, yerr=2 * exp_value_std * exp_value_mean,
                 label='Data', color="k", capsize=3, capthick=1, markersize=3)

    plt.grid(True, which="both", ls=":")
    plt.legend(loc='best')
    plt.xlim(1, 250)
    plt.ylim(bottom=1)
    plt.ylim(top=10000)
    plt.savefig('endend', bbox_inches='tight', dpi=300)

    # Create fit for gyraton radius.
    q1 = np.polyfit(np.log(a[1::]), np.log(rad_gyr_mean[1::]), 1)
    print('Fitting parameters for gyration radius -> ', q1)
    xx1 = np.linspace(0, 250, 1000)
    yy1 = np.exp(q1[1]) * xx ** q1[0]

    # Plot gyraton radius.
    plt.figure(2)
    plt.xscale("log")
    plt.yscale("log")
    plt.plot(xx1, yy1, 'r--', label="Fit", linewidth=3)
    plt.xlabel(r'$N_{beads}$')
    plt.ylabel(r'$R_g^2$ $[\sigma^2]$')
    plt.xscale("log")
    plt.yscale("log")
    plt.errorbar(a, rad_gyr_mean, fmt='x', xerr=None, yerr=rad_gyr_std, label='Data', color="k", capsize=3, capthick=1,
                 markersize=3)
    plt.grid(True, which="both", ls=":")
    plt.legend(loc='best')
    plt.xlim(1, 250)
    plt.ylim(bottom=0.1)
    plt.ylim(top=1000)
    plt.savefig('gyros', bbox_inches='tight', dpi=300)

    # Plot population size
    plt.figure(3)
    plt.title('Population size')
    plt.plot(np.arange(2, n_beads, 1), popsize[2:])
    plt.grid(True, which="both", ls="-")
    plt.ylabel('Population size')
    plt.xlabel('N (Number of beads)')
    plt.show()


def bootstrap(polweight_array, exp_value_array, n_beads):
    """Preforms bootstrap onto end-to-end distances of polymers to calculate the error

    Parameters:
    ----------
    polweight_array: array of size (n_beads, n_poll + amount of enriching - 0.5 times amount of pruning)
        Array with all weights of all beads of all polymers
    exp_value_array: array of size (n_beads, n_poll + amount of enriching - 0.5 times amount of pruning)
        Array at which the bootstrap is applied
    n_beads: integer > 2
        Number of maximum beats in a polymer

    Results:
    --------
    exp_value_mean: array of size (n_beads, 1)
        Mean value of end-to-end distance per number of beads
    exp_value_std: array of size (n_beads, 1)
        Standard deviation of end-to-end distances per number of beads
    """

    # Initialize/allocate values
    nn = 100      # Amount of bootstraps (n)
    a = exp_value_array.shape
    exp_value = np.zeros((a[0] - 1, nn))  # Allocate observable
    j = 0
    m = 0
    while j < nn:
        # Make random array of size with values ranging from 0 to max(index variable)
        c = np.random.randint(a[1], size=(a[1]))

        # Get random values from variables using random array c
        exp_value_bootstrap = exp_value_array[:, c]
        polweight_bootstrap = polweight_array[:, c]

        # Calculate true observable values
        exp_value_times_bootstrap = np.multiply(exp_value_bootstrap, polweight_bootstrap)
        sum_exp_value_times_bootstrap = np.sum(exp_value_times_bootstrap, axis=1)
        sum_weights_bootstrap = np.sum(polweight_bootstrap, axis=1)

        # If statements which warns the user when data is not sufficient
        if np.count_nonzero(sum_weights_bootstrap) == n_beads:
            exp_value[:, j] = np.divide(sum_exp_value_times_bootstrap[0:n_beads - 1],
                                        sum_weights_bootstrap[0:n_beads - 1])
            j = j + 1
        elif np.count_nonzero(exp_value_array[n_beads - 1, :]) == 0:
            print('No polymer reached ', n_beads, ' beads, try again, or lower variable n_beads.')
            exit()
        elif m > 500:
            print('Not enough polymers reached ', n_beads, ' beads, try again, or adjust variable n_beads.')
            exit()
        else:
            m = m + 1                     

    # Calculate mean and standard deviation of observable
    exp_value_mean = np.mean(exp_value, axis=1)
    exp_value_std = np.std(exp_value, axis=1)
    return exp_value_mean, exp_value_std


def polsize(end_end_array):
    """Calulates the population size of polymers for different lenghts

    Parameters:
    ----------
    end_end_array: array of size (n_beads, n_poll + amount of enriching - 0.5 times amount of pruning)
        Distance of bead to center bead, for every bead of every simulated polymer.

    Results:
    --------
    popsize: array of size (n_beads)
        population size of polymers for different lenghts    
    """
    # Count nonzero elements
    popsize = np.count_nonzero(end_end_array, axis=1)

    return popsize


def radiusgyration(pos_array):
    """Calulates the radius of gyration.

    Parameters:
    ----------
    pos_array: array of size (n_beads, ?)
        Array with positions of the beads of polymers which stopped growing
    Results:
    --------
    rad_gyr: array of size (n_beads, n_poll + amount of enriching - 0.5 times amount of pruning)
        Array containing the radius of gyration for all polymers at different lenghts
    """

    # Preparation for calculation can start (allocation, initialization)
    pos_array = np.delete(pos_array, [0, 1], axis=1)
    [imax, jmax] = pos_array.shape
    rad_gyr = np.zeros([imax, int(jmax / 2)])
    j = 0

    # Loop over all polymers
    while j < jmax:
        i = 1
        imax = np.count_nonzero(pos_array[:, j], axis=0)
        # Loop over all beads
        while i < imax:
            # Calculate radius of gyration
            N = i + 1

            xk = pos_array[:i + 1, j]
            yk = pos_array[:i + 1, j + 1]

            xmean = np.sum(xk) / N
            ymean = np.sum(yk) / N

            xr = xk - xmean
            yr = yk - ymean

            rad_gyr[i, int(j / 2)] = 1 / N * np.sum(np.power(xr, 2) + np.power(yr, 2))

            i = i + 1
        j = j + 2

    # Adjust array to get it fit for bootstrap function
    rad_gyr_zeros = np.zeros(rad_gyr.shape[0])
    rad_gyr = np.insert(rad_gyr, 0, rad_gyr_zeros, axis=1)

    return rad_gyr